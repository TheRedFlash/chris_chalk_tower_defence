﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The Entire class is one big Get and Set method but is only Set via the XML Loader
public class TowerObject {

    private float strength;
    private float range;
    private float fireRate;
    private int cost;
    private string modelName;
    private float bulletspeed;
    private GameObject towerPrefab;
    private string specialEffect;

    public TowerObject(float _strength, float _range, float _fireRate, int _cost, string _model, float _bulletspeed, string _specialEffect, string inner_text)
    {
        this.strength = _strength;
        this.range = _range;
        this.fireRate = _fireRate;
        this.cost = _cost;
        this.modelName = _model;
        this.bulletspeed = _bulletspeed;
        this.specialEffect = _specialEffect;
        this.towerPrefab = Resources.Load<GameObject>("Prefabs/" + _model);
    }

    public float getStrength()
    {
        return strength;
    }

    public float getRange()
    {
        return range;
    }

    public float getfireRate()
    {
        return fireRate;
    }

    public int getCost()
    {
        return cost;
    }

    public string getModelName()
    {
        return modelName;
    }

    public float getbulletSpeed()
    {
        return bulletspeed;
    }

    public GameObject gettowerPrefab()
    {
        return towerPrefab;
    }

    public string getSpecialEffect()
    {
        return specialEffect;
    }
}
