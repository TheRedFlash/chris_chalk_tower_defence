﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class VariableManager : MonoBehaviour {

	public static VariableManager instance;

    public Scene thisScene;

    bool buttonactive = true;
    public bool gamePaused = false;

    //Player Variables
    public static int playerHP;
    public static int goldCollected;

    //NPC Variables
    public static int noOfNPCS; //How many enemies are alive
    public static int noOfNPCsSpawned; //How many enemies that have been spawned this wave
    public static int noOfNPCsToSpawn; // How many enemies to be spawned this wave
	public static int wavesleft;

    //UI Variables
    public static string towerBeingBuilt;
	public static string difficulty;
	bool gameWon;
    public GameObject pauseMenu;
    public GameObject cancelConstructionButton;

    //UI to display variables
    public Text goldCollectedText;
    public Text wavesLeftText;
    public Text playerHPText;

    //FSM for building
    public static BuildingModeState curState;
	public enum BuildingModeState
	{
		None,
		Tower,
        Sell
	}

    void Start() {

        ResetGame();
		
        //Alerts developer if there is more than 1 intsance of variable manager
		if (instance != null) 
		{
			Debug.LogWarning ("More than one Instance of VariableManager");
			return;
		} 
		else 
		{
			instance = this;
		}
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            PauseGame();

        Time.timeScale = (!gamePaused) ? 1.00f : 0.00f;

        SetDisplayTextBoxes();

        CheckGameCondition();

        if (wavesleft <= 0 && buttonactive == true)
        {
            buttonactive = false;
            foreach (GameObject gameObject in Resources.FindObjectsOfTypeAll(typeof(GameObject)))
                if (gameObject.name == "WaveSpawner")
                    gameObject.SetActive(false);
        }
        else if (wavesleft > 0 && buttonactive == false)
        {
            buttonactive = true;
            foreach (GameObject gameObject in Resources.FindObjectsOfTypeAll(typeof(GameObject)))
                if (gameObject.name == "WaveSpawner")
                    gameObject.SetActive(true);
        }

        if (curState == BuildingModeState.None)
        {
            cancelConstructionButton.SetActive(false);
        }
        else if (curState == BuildingModeState.Tower)
            cancelConstructionButton.SetActive(true);
    }

    public void PauseGame()
    {
        gamePaused = !gamePaused;

        pauseMenu.SetActive(!pauseMenu.activeSelf);
    }

    //Sets the UI Stat boxes in one neat function
    private void SetDisplayTextBoxes()
    {
        goldCollectedText = GameObject.Find("GoldCollected").GetComponent<Text>();
        wavesLeftText = GameObject.Find("WavesLeft").GetComponent<Text>();
        playerHPText = GameObject.Find("PlayerHP").GetComponent<Text>();
        Text currentMode = GameObject.Find("CurrentMode").GetComponent<Text>();
        goldCollectedText.text = "Gold: " + goldCollected;
        wavesLeftText.text = "Waves Left:" + wavesleft;
        playerHPText.text = "Health Points:" + playerHP;
        currentMode.text = "Mode: " + curState;
    }

    //If the game is meant to end this will control it
    private void CheckGameCondition()
    {
        //If the Player Dies the game is Reset, the condition is set to lose and the scene is changed to the endgame screen
        if (playerHP <= 0)
        {
            PlayerPrefs.SetInt("WavesLeft", wavesleft);
            PlayerPrefs.SetString("EndGameCondition", "Lose");
            ResetGame();
            SceneManager.LoadScene(2);
        }
        //If there are no more waves, the game checks for NPCs and once the list is empty the game is won
        if (wavesleft <= 0 && playerHP > 0 && VariableManager.noOfNPCsSpawned == VariableManager.noOfNPCsToSpawn)
        {
            GameObject[] anEnemy = GameObject.FindGameObjectsWithTag("Enemy");
            if (anEnemy.Length <= 0)
            {
                PlayerPrefs.SetString("EndGameCondition", "Win");
                PlayerPrefs.SetInt("PlayerHealth", playerHP);
                PlayerPrefs.SetInt("GoldCollected", goldCollected);
                ResetGame();
                if (thisScene.name == "Game_Scene")
                    SceneManager.LoadScene(3);
                else
                    SceneManager.LoadScene(2);
            }
        }
    }

    //Sets the game to the base values
	public void ResetGame()
	{
        GameObject[] listOfEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject[] listOfTowers = GameObject.FindGameObjectsWithTag("Tower");
        thisScene = SceneManager.GetActiveScene();


        //Sets the difficulty set by the player, "Easy" is the default if no string is detected
        difficulty = PlayerPrefs.GetString("Difficulty", "Easy");

        //If for some reason the above fails and there is no entry at all, it sets to easy
        if (difficulty == null)
            difficulty = "Easy";

        curState = BuildingModeState.None; //The default state of the player when the scene loads

        //Sets the game stats based on which scene it is in
        if (thisScene.name == "Game_Scene")
        {
            //Sets the ammount of waves and hp based on difficulty
            if (difficulty == "Easy")
            {
                wavesleft = 5;
                playerHP = 10;
                goldCollected = 100;
            }
            else
            {
                wavesleft = 10;
                playerHP = 15;
                goldCollected = 200;
            }
        }
        else
        {
            if (difficulty == "Easy")
            {
                wavesleft = 10;
                playerHP = 20;
                goldCollected = 200;
            }
            else
            {
                wavesleft = 15;
                playerHP = 25;
                goldCollected = 600;
            }
        }

        noOfNPCS = 0;
        noOfNPCsSpawned = 0;
        noOfNPCsToSpawn = 0;

        towerBeingBuilt = "Tower1"; //Defaults to the basic tower just incase but is set during gameplay

        SetDisplayTextBoxes();

        foreach (GameObject gameObject in listOfEnemies)
            Destroy(gameObject);
        foreach (GameObject gameObject in listOfTowers)
            Destroy(gameObject);
        if (gamePaused == true)
        {
            gamePaused = false;
            pauseMenu.SetActive(false);
        }            
    }

    public void CancelConstruction()
    {
        goldCollected = goldCollected + ShopStartup._towerObject.getCost();
        curState = BuildingModeState.None;
    }

    public void SellTowerMode()
    {
        if (curState == BuildingModeState.None)
            curState = BuildingModeState.Sell;
        else if (curState == BuildingModeState.Sell)
            curState = BuildingModeState.None;
    }
}
