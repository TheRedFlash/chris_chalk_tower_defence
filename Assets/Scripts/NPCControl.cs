﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class NPCControl : MonoBehaviour {

    //Enemy Stats
    public float npcHP;
    public int bounty;
    public bool slowEffectApllied = false;
    public float npcSpeed;

    //NavMesh and Navigation Realted Variables
    public UnityEngine.AI.NavMeshAgent agent;
    private GameObject[] checkpoints;
    public int currentPoint = 0;
    
    public Scene thisScene;

    public FSMState curState;
    public enum FSMState
	{
		Death,
		Travel
	}

	void Start ()
    {
        thisScene = SceneManager.GetActiveScene();
        OnSpawn();
    }

    void Update () 
	{
		FSMUpdate ();

        if (npcHP <= 0)
            curState = FSMState.Death;
	}

    //Does everything an object would do on Start but in a function for cleanliness sake
    public void OnSpawn()
    {
        //If its level 2 which has 2 paths, chooses a path
        if (thisScene.name == "Game_Scene_2")
        {
            int rand = Random.Range(0, 2);
            if (rand == 0)
                checkpoints = GameObject.FindGameObjectsWithTag("Checkpoint").OrderBy(go => go.name).ToArray();
            else
                checkpoints = GameObject.FindGameObjectsWithTag("CheckpointA").OrderBy(go => go.name).ToArray();
        }
        else
            checkpoints = GameObject.FindGameObjectsWithTag("Checkpoint").OrderBy(go => go.name).ToArray(); //Finds any checkpoints in the scene and puts them in the array in order of name
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        curState = FSMState.Travel; //Makes sure when the NPC spawns it starts travelling
        SetStats();
    }

    //Holds the enemy's base stats, these could be set via XML //Allows the function to be called for potential future usage
    private void SetStats()
    {
        if (PlayerPrefs.GetString("Difficulty") == "Easy")
        {
            npcHP = 5;
            bounty = 25;
        }
        else
        {
            npcHP = 10;
            bounty = 25;
        }
        npcSpeed = 1;
        agent.speed = npcSpeed;
    }

    public void FSMUpdate()
	{
        //Due to being called in update, every frame it'll do the function depending on what state its in, useful as it keeps the update function clean
		switch (curState) 
		{
		case FSMState.Travel:
			UpdateTravelState ();
			break;
		case FSMState.Death:
			UpdateDeathState();
			break;
		}
	}

	void UpdateTravelState() 
	{
        //If it reaches the end, kills it
        if (Vector3.Distance(transform.position, checkpoints[(checkpoints.Length - 1)].transform.position) <= 0.4f)
            curState = FSMState.Death;
        //Sets the destination to the next checkpoint
        if (currentPoint < checkpoints.Length)
            agent.SetDestination(checkpoints[currentPoint].transform.position);
        //When it reaches a checkpoint, allows it to find the next one
        if (Vector3.Distance(transform.position, checkpoints[currentPoint].transform.position) <= 0.3f)
            currentPoint++;     
    }

    //Having both methods of death in the same means that if the player would kill the enemy in the same frame it would reach the end it always checks it's hp first
	void UpdateDeathState(){
        //If the NPC is killed by a tower
        if (npcHP <= 0)
        {
            VariableManager.noOfNPCS = VariableManager.noOfNPCS - 1;
            VariableManager.goldCollected = VariableManager.goldCollected + bounty;
            Destroy(gameObject);
        }
        //This happens if the NPC gets to the end instead of being killed by the player
        else
        {
            VariableManager.playerHP = VariableManager.playerHP - 1;
            VariableManager.noOfNPCS = VariableManager.noOfNPCS - 1;
            Destroy(gameObject);
        }
	}
}
