﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonMouseOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    public Text tooltip;

    private void Start()
    {
        tooltip = GameObject.FindGameObjectWithTag("ToolTip").GetComponent<Text>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (transform.name == "BasicTower")
            tooltip.text = "Basic Tower" + Environment.NewLine + Environment.NewLine + "Nothing Special";
        if (transform.name == "SlowTower")
            tooltip.text = "Slow Tower" + Environment.NewLine + Environment.NewLine + "Does no damage, but slows the enemy's movement";
        if (transform.name == "StrongTower")
            tooltip.text = "Strong Tower" + Environment.NewLine + Environment.NewLine + "Does increased damage but shoots less often";
        if (transform.name == "MinigunTower")
            tooltip.text = "Minigun Tower" + Environment.NewLine + Environment.NewLine + "Shoots at a much faster rate but does less damage";
        if (transform.name == "SniperTower")
            tooltip.text = "Sniper Tower" + Environment.NewLine + Environment.NewLine + "Has a much larger range than most turrets, deals increased damage but fires much less often";
        if (transform.name == "InstaKillTower")
            tooltip.text = "InstantKill Tower" + Environment.NewLine + Environment.NewLine + "Instantly kills the enemy but has a very high cooldown";
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tooltip.text = null;
    }
}
