﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//The Class Acts as a general menu control script via the SceneLoader method
public class MainMenuControl : MonoBehaviour {

	public string difficulty;
	public Text textDifficulty;

    public Scene thisScene;

	void Start () {
        thisScene = SceneManager.GetActiveScene();
        if (thisScene.buildIndex == 0)
		    SetDifficulty ("Easy");
    }

    //Used to change the difficulty, PlayerPrefs is a global script where you can add a variable and set it. 
    //It is useful when you have a variable you want to have access to in multiple scenes
	public void SetDifficulty (string _difficulty){
		difficulty = _difficulty;
		PlayerPrefs.SetString ("Difficulty" ,difficulty);
        textDifficulty.text = difficulty;
    }

	public void SceneLoader(int i){
		SceneManager.LoadScene (i);
	}

    public void CancelQuit()
    {
        foreach (GameObject gameObject in Resources.FindObjectsOfTypeAll(typeof(GameObject)))
            if (gameObject.name == "QuitConfirmation")
                gameObject.SetActive(false);
    }

    //TODO Add a confirmation window
	public void QuitGame(int quitStage){
        
        
        if(quitStage == 0)
        {
            foreach (GameObject gameObject in Resources.FindObjectsOfTypeAll(typeof(GameObject)))
                if (gameObject.name == "QuitConfirmation")
                    gameObject.SetActive(true);
        }
        else
        {
            Debug.Log("Game Quit");
            Application.Quit();
        }
            
	}
}
