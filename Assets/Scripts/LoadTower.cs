﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;  //Lets us use Lists
using System.Xml;  //Basic XML Attributes
using System.Xml.Serialization;  //File Management
using System.IO;


public class LoadTower : MonoBehaviour {

    public static List<TowerObject> towers = new List<TowerObject>();
    public TowerObject currentTower { get; set; }

	void Awake ()
    {
        //If there are more than 1 instances of the script it'll delete themselves bar 1
        if (FindObjectsOfType<LoadTower>().Length > 1)
            Destroy(this.gameObject);
        //If the towers list is empty on startup, this will fill it
        if (towers.Count == 0)
            loadXML();
	}
	
    private void loadXML()
    {
        //Loads the XML raw text file
        TextAsset textData = Resources.Load<TextAsset>("TowerData");
        //Sends the text file to a function that will convert the nodes
        parseTowerDataXML(textData.text);
    }

    private void parseTowerDataXML(string xmlData)
    {
        //Creates a new reference to the xml document and then loads it
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(new StringReader(xmlData));

        //Tells the function where to look in the XML for the following nodes, xmlPathPattern can be changed to different parts if the XML contains other sections
        string xmlPathPattern = "//towers/tower";
        XmlNodeList myNodeList = xmlDoc.SelectNodes(xmlPathPattern);

        //For each line in the XML section specified it takes each attribute and converts into the relevant variable
        foreach (XmlNode node in myNodeList)
        {                                           //int.Parse is required to convert the attribute from a string to an int                 //As we can see it can be any non-string variable
            TowerObject towerObject = new TowerObject(float.Parse(node.Attributes["strength"].Value), float.Parse(node.Attributes["range"].Value), float.Parse(node.Attributes["rate"].Value), int.Parse(node.Attributes["cost"].Value), node.Attributes["model"].Value, float.Parse(node.Attributes["bulletspeed"].Value), node.Attributes["specialEffect"].Value, node.InnerText);
            towers.Add(towerObject);
        }
    }
}
