﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAI : MonoBehaviour {
    
    //Tower Stats assigned via xml
    public float strength;
    public float range;
    public int cost;
    public float rate;
    public float bulletSpeed;
    public string specialEffect;

    public float turnSpeed = 10f;
    private float fireCountdown = 0f;

	private Transform firePoint; //Where the bullet comes from
	private Transform target;

    void Awake ()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f); //Causes Update target to be ran every second starting from the beginning of itd existence
        SetTowerStats();
    }

	void Update ()
    {
        //Doesn't go any further in the fucntion if there is no target
        if (target == null)
            return;

        LookAtEnemy();

        //Makes sure there is a delay between shots and shoots if the cooldown is met
        if (fireCountdown <= 0f)
        {
            Shoot();
        }
        //Decreases the countdown ever frame
        fireCountdown -= Time.deltaTime;
    }

    //Makes sure it has the correct target
    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy"); //Gets a list of all enemies in the scene
        List<GameObject> enemiesInRange = new List<GameObject>();
        int furthestPoint = 0;
        

        //For every enemy currently in the scene calculates which are within range
        foreach (GameObject enemy in enemies)
        { 
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);

            //Makes A list of all enemies within the tower's range
            if (distanceToEnemy <= range)
            {
                enemiesInRange.Add(enemy); //No Need to remove as a new list is made every 0.5 seconds due to the invoke repeating
            }
            else if (distanceToEnemy > range) //If the enemy is out of range it sets the target to nothing
                target = null;
        }
        //Compares every enemy's current checkpoint index and the one with the largest that is within range will be set as a target
        foreach (GameObject enemy in enemiesInRange)
        {
            int targetsPoint = enemy.GetComponent<NPCControl>().currentPoint;
            if (targetsPoint > furthestPoint)
            {
                furthestPoint = targetsPoint;
                target = enemy.transform;
            }
        }
    }

    //Uses the reference from ShopStartup to assign the correct variables
    private void SetTowerStats()
    {
        strength = ShopStartup._towerObject.getStrength();
        range = ShopStartup._towerObject.getRange();
        cost = ShopStartup._towerObject.getCost();
        rate = ShopStartup._towerObject.getfireRate();
        bulletSpeed = ShopStartup._towerObject.getbulletSpeed();
        specialEffect = ShopStartup._towerObject.getSpecialEffect();
        firePoint = this.transform.Find("FirePoint");
        fireCountdown = 0.5f; //We set the countdown here to give the turret time to turn before shooting the first shot
    }
    //Sets up direction and rotation for the turret to point to the enemy
    private void LookAtEnemy()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    //Shoots the bullet
    void Shoot()
	{
		GameObject bulletGO = Instantiate ((Resources.Load<GameObject>("Prefabs/Bullet")), firePoint.position, firePoint.rotation);
		Bullet bullet = bulletGO.GetComponent<Bullet> ();
		bullet.bulletDamage = strength; //Sets the bullet damage
        bullet.speed = bulletSpeed; //Sets speed of the bullet
        bullet.specialEffect = specialEffect; //Sets the specialEffect of the bullet

		if (bullet != null)
			bullet.Seek (target); //Sets the bullet's target

        fireCountdown = rate; /// 1f;
    }

    //Adds a circle to see the range of the turrets in the editor. Useful for finding the correct range the tower should have
	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (transform.position, range);
	}
}
