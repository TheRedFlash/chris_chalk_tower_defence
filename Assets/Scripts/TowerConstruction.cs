﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerConstruction : MonoBehaviour {

    bool towerBuilt = false;

    void OnMouseDown()
    {
        if (VariableManager.curState == VariableManager.BuildingModeState.Tower && towerBuilt == false)
        {
            GameObject tower = Instantiate((Resources.Load<GameObject>("Prefabs/" + VariableManager.towerBeingBuilt)), this.transform.position, Quaternion.identity) as GameObject;
            tower.transform.parent = this.transform;
            tower.AddComponent<TowerAI>();
            towerBuilt = true;
            VariableManager.curState = VariableManager.BuildingModeState.None;
        }
        else if (VariableManager.curState == VariableManager.BuildingModeState.Sell && towerBuilt == true)
        {
            VariableManager.goldCollected = VariableManager.goldCollected + this.GetComponentInChildren<TowerAI>().cost;
            Destroy(this.transform.GetChild(0).gameObject);
            towerBuilt = false;
            VariableManager.curState = VariableManager.BuildingModeState.None;
        }
    }
}
