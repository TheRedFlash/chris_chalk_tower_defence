﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	private Transform target; //Sets a target for the Bullet to travel to
	public float bulletDamage; //Bullet Damage, needs to be public as TowerAI and other scripts can affect it
    public string specialEffect; //Sets whatever special effect the bullet should have

    public float speed = 0.2f; //Bullet Speed

    public void Seek (Transform _target)
	{
		target = _target; //Allows us to change a Bullet's target without accessing the direct variable
	}

	void Update () 
	{
        //If the target is destroyed before the bullet gets there (if multiple turrets target one enemy) it'll be destroyed
		if (target == null) 
		{
			Destroy (gameObject);
			return;
		}

		Vector3 dir = target.position - transform.position; //Keeps the bullet facing the enemy
		float distanceThisFrame = speed * Time.deltaTime; //Determines how far the bullet will move this frame
		if (dir.magnitude <= distanceThisFrame)  //Used to determine if the bullet would go through the enemy due to speed, helps reduce bugs of the bullet travelling past the enemy
        {
			HitTarget (specialEffect);
			return;
		}

        //Moves the bullet
		transform.Translate ((dir.normalized * distanceThisFrame), Space.World);
	}

	void HitTarget(string _effect)
	{
        //Gets the specific instance of NPC Control from the enemy it's gonna hit
		NPCControl npcControl = target.GetComponent<NPCControl> ();
        npcControl.npcHP = npcControl.npcHP - bulletDamage;

        //Different things happen depending on what the effect is
        if (_effect == "None")
        {
            Destroy(gameObject);
        }
        if (_effect == "Slow")
        {
            if (npcControl.slowEffectApllied == false)
                npcControl.agent.speed = npcControl.agent.speed / 2;
            StartCoroutine(EffectDuration(_effect, npcControl));
            npcControl.slowEffectApllied = true;
            GetComponent<MeshRenderer>().enabled = false;
        }
	}

    //Determines how long the effect should last
    IEnumerator EffectDuration(string _text, NPCControl npcControl)
    {
        if (_text == "Slow")
        {
            Debug.Log(specialEffect);
            yield return new WaitForSeconds(5);
            Debug.Log(specialEffect);
            npcControl.agent.speed = npcControl.npcSpeed;
            npcControl.slowEffectApllied = false;
            Destroy(gameObject);
        }
    }
}
