﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ShopStartup : MonoBehaviour {

    //Construction variables
    public GameObject buttonTemplate;
    public GameObject shopPanel;

    //Warning Pop-ups
    public GameObject alreadyBuldingWarning;
    public GameObject insufficentFundsWarning;

    public static TowerObject _towerObject; //Public and static as other scripts access it

	// Use Start as XML list needs to be built first on Awake in LoadTower
	void Start ()
    {
        ShopInstatiation();
    }
	
	public void ShopInstatiation()
    {
        foreach (TowerObject obj in LoadTower.towers) //Runs once for each script in the list and can refrence the stats of that script
        {
            GameObject copy = Instantiate(buttonTemplate); //Creates a copy of a button
            copy.name = obj.getModelName(); //Sets the new button's name to that of the xml model name, Useful for other methods
            copy.transform.SetParent(shopPanel.transform); //Sets that Button's parent to the panel
            copy.transform.localScale = new Vector3(1, 1, 1); //Sets that buttons scale to a uniform number
            copy.GetComponentInChildren<Text>().text = (obj.getModelName() + Environment.NewLine + "Cost: " + obj.getCost()).ToString(); //Get the text component of the button and changes it to the model name from the xml
            copy.transform.Find("Image").GetComponent<Image>().sprite = Resources.Load<Sprite>("TowerImages/" + obj.getModelName()); //Finds the corresponding image and applies it
            copy.AddComponent<ButtonMouseOver>(); //Adds the script for tooltips

            copy.GetComponent<Button>().onClick.AddListener(
                () =>//Where it says ButtonClicked is where the function to build go to a tower building mode should go
                {
                    if (VariableManager.goldCollected >= obj.getCost() && VariableManager.curState == VariableManager.BuildingModeState.None)
                    {
                        _towerObject = obj;
                        VariableManager.curState = VariableManager.BuildingModeState.Tower;
                        VariableManager.goldCollected = VariableManager.goldCollected - obj.getCost();
                        VariableManager.towerBeingBuilt = obj.getModelName();
                    }
                    else if (VariableManager.curState == VariableManager.BuildingModeState.Tower)
                    {
                        alreadyBuldingWarning.SetActive(true);
                        StartCoroutine(TextDelay("BuildingWarning"));
                    }
                    else
                    {
                        insufficentFundsWarning.SetActive(true);
                        StartCoroutine(TextDelay("GoldWarning"));
                    }      
                }
             );
        }
    }

    //Allows us to cause a delay for the text, setup so that we can do different things on this delay depending on what string we give it
    IEnumerator TextDelay(string _text)
    {
        if (_text == "BuildingWarning")
        {
            yield return new WaitForSeconds(2);
            alreadyBuldingWarning.SetActive(false);
        }
        if(_text == "GoldWarning")
        {
            yield return new WaitForSeconds(2);
            insufficentFundsWarning.SetActive(false);
        }
    }

}
