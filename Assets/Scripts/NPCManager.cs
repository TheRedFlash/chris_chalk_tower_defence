﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCManager : MonoBehaviour {

	private float spawnTime = 1;
	public bool firstWaveSpawned = false;

    public GameObject waveSpawningWarning;

    public void Wave ()
	{

        if (CheckWaveStatus() == true)
            return;

        //When a wave is spawned the number of waves left goes down
        VariableManager.wavesleft = VariableManager.wavesleft - 1;

        //Different Difficulties have different wave mechanics
        switch (VariableManager.difficulty)
        {
			case "Easy":
				VariableManager.noOfNPCsToSpawn++;
				if (firstWaveSpawned == false) //Redundant in this difficulty but is here for future development
					firstWaveSpawned = true;
				InvokeRepeating ("Spawn", spawnTime, spawnTime);
				break;
			case "Hard":
				VariableManager.noOfNPCsToSpawn = VariableManager.noOfNPCsToSpawn + 2;
				if (firstWaveSpawned == false)
                {
					VariableManager.noOfNPCsToSpawn = 2;
					firstWaveSpawned = true;
				}
				InvokeRepeating ("Spawn", spawnTime, spawnTime);
				break;
		}
	}

    //Used to spawn the NPCs
	void Spawn()
	{
		if (VariableManager.noOfNPCsSpawned < VariableManager.noOfNPCsToSpawn) 
		{
            GameObject enemy = Instantiate((Resources.Load<GameObject>("Prefabs/Enemy")), this.transform.position, Quaternion.identity) as GameObject;
            enemy.AddComponent<NPCControl>();            
			VariableManager.noOfNPCsSpawned++;
            VariableManager.noOfNPCS++;
		}
		if (VariableManager.noOfNPCsSpawned >= VariableManager.noOfNPCsToSpawn) 
		{
			CancelInvoke();
		}
	}

    //Used to not adavance the Wave method if it should return
    bool CheckWaveStatus()
    {
        bool shouldReturn = false;

        //Stops wave being spawned if the current one hasn't finished
        if (VariableManager.noOfNPCsSpawned < VariableManager.noOfNPCsToSpawn)
        {
            waveSpawningWarning.SetActive(true);
            StartCoroutine(TextDelay("SpawningWarning"));
            shouldReturn = true;
            return shouldReturn;
        }

        //Allows Wave() to continue as the wave is finished and resets the enemies to spawn
        if (VariableManager.noOfNPCsSpawned == VariableManager.noOfNPCsToSpawn)
        {
            VariableManager.noOfNPCsSpawned = 0;
            shouldReturn = false;
        }

        return shouldReturn;
    }

    IEnumerator TextDelay(string _text)
    {
        if (_text == "SpawningWarning")
        {
            yield return new WaitForSeconds(2);
            waveSpawningWarning.SetActive(false);
        }
    }
}
