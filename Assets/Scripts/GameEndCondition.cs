﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEndCondition : MonoBehaviour {

    private Text endGameText;

    private string endGameCondition;

	void Start () {
        //Depending on how the game was ended changes what appears on the end screen
        endGameCondition = PlayerPrefs.GetString("EndGameCondition");
        endGameText = this.GetComponent<Text>();
        SetText();
	}

    void SetText()
    {
        if(endGameCondition == "Win")
        {
            endGameText.text = ("Congratulations, You Won!" + Environment.NewLine + "HP Remaining: " + PlayerPrefs.GetInt("PlayerHealth") + Environment.NewLine + "Gold Collected: " + PlayerPrefs.GetInt("GoldCollected"));
        }
        else if (endGameCondition == "Lose")
        {
            endGameText.text = ("You were defeated!" + Environment.NewLine + "Try Again?" + Environment.NewLine + "Waves Remaining: " + PlayerPrefs.GetInt("WavesLeft"));
        }
    }
}
